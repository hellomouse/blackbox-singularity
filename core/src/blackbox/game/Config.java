package blackbox.game;

public final class Config {
    public static final int WINDOW_WIDTH = 1920 / 2;
    public static final int WINDOW_HEIGHT = 1080 / 2;
    public static final String WINDOW_TITLE = "Blackbox: Singularity";
    public static final String MENU_NOTE = "v1.0.0 Bowserinator 2018";
}
