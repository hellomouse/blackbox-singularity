package blackbox.game.util;

import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * Util class for drawing text
 * onto the screen
 */
public class DrawText {
    /**
     * Construct a DrawText class. Private because
     * this class should not be constructable.
     */
    private DrawText() {}

    public static void drawTextRect(String text, BitmapFont font, int x, int y, int w, int h) {

    }
}
